c.completion.cmd_history_max_items = 100
c.completion.delay = 0
c.completion.height = '40%'
c.completion.min_chars = 1
c.completion.quick = True
c.completion.show = 'always'
c.completion.shrink = False
c.completion.use_best_match = False
c.completion.timestamp_format = '%Y-%m-%d'
c.completion.web_history.max_items = -1 # unlimited
c.completion.open_categories = ["quickmarks", "bookmarks", "history"]
c.completion.scrollbar.padding = 1
c.completion.scrollbar.width = 8

c.fonts.completion.category = 'bold 10pt monospace'
c.fonts.completion.entry = '10pt monospace'
c.fonts.debug_console = '10pt monospace'
c.fonts.downloads = '10pt monospace'
c.fonts.hints = 'bold 10pt monospace'
c.fonts.keyhint = '10pt monospace'
c.fonts.messages.error = '10pt monospace'
c.fonts.messages.info = '10pt monospace'
c.fonts.messages.warning = '10pt monospace'
#c.fonts.monospace = '"xos4 Terminus", Terminus, Monospace, "DejaVu Sans Mono", Monaco, "Bitstream Vera Sans Mono", "Andale Mono", "Courier New", Courier, "Liberation Mono", monospace, Fixed, Consolas, Terminal'
c.fonts.prompts = '10pt sans-serif'
c.fonts.statusbar = '10pt monospace'
c.fonts.tabs = '10pt monospace'
c.fonts.web.family.cursive = ''
c.fonts.web.family.fantasy = ''
c.fonts.web.family.fixed = ''
c.fonts.web.family.sans_serif = ''
c.fonts.web.family.serif = ''
c.fonts.web.family.standard = ''
c.fonts.web.size.default = 16
c.fonts.web.size.default_fixed = 13
c.fonts.web.size.minimum = 0
c.fonts.web.size.minimum_logical = 6

c.keyhint.delay = 0
c.keyhint.radius = 0

c.prompt.filebrowser = True
c.prompt.radius = 0

c.scrolling.bar = 'always'
c.scrolling.smooth = False

c.statusbar.hide = True
c.statusbar.widgets = ['keypress', 'url', 'scroll_raw', 'progress']
c.statusbar.padding = {'top': 1, 'bottom': 1, 'left': 0, 'right': 0}
c.statusbar.position = 'bottom'

c.tabs.background = False
c.tabs.close_mouse_button = 'middle'
c.tabs.close_mouse_button_on_bar = 'new-tab'
c.tabs.favicons.scale = 1.0
c.tabs.favicons.show = 'never'
c.tabs.indicator.padding = {'top': 2, 'bottom': 2, 'left': 0, 'right': 5}
c.tabs.indicator.width = 5
c.tabs.last_close = 'ignore'
c.tabs.mousewheel_switching = True
c.tabs.new_position.related = 'next'
c.tabs.new_position.unrelated = 'last'
c.tabs.padding = {'top': 1, 'bottom': 1, 'left': 5, 'right': 5}
c.tabs.mode_on_change = 'normal'
c.tabs.pinned.shrink = True
c.tabs.position = 'top'
c.tabs.select_on_remove = 'next'
c.tabs.show = 'never'
c.tabs.show_switching_delay = 800
c.tabs.tabs_are_windows = False
c.tabs.title.alignment = 'left'
c.tabs.title.format = '{index}.{perc_raw}| {current_title}{audio}'
c.tabs.title.format_pinned = '{index}'
c.tabs.width = '15%'
c.tabs.wrap = True

c.url.auto_search = 'naive'
c.url.default_page = '~/.config/qutebrowser/start.html'

c.url.searchengines.update({
                       'w'      : 'https://en.wikipedia.org/?search={}',
                       })

c.url.start_pages = ['~/.config/qutebrowser/start.html']

c.window.title_format = ' '
c.window.hide_decoration = True

c.zoom.default = '100%'
c.zoom.levels = ['25%', '33%', '50%', '67%', '75%', '90%', '100%', '110%', '125%', '150%', '175%', '200%', '250%', '300%', '400%', '500%']
c.zoom.mouse_divider = 512

# toggle ui (eXchange)
config.bind('xf', 'fullscreen')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xb', 'config-cycle statusbar.hide')
config.bind('xp', 'set tabs.show always;; later {} set tabs.show never'.format(c.tabs.show_switching_delay)) # peek tab bar
config.bind('xx', 'config-cycle tabs.position left top')
config.bind('<F1>', 'spawn -u "~/.config/qutebrowser/sh/tron.sh" ;; config-source "py/tron.py" ;; config-unset content.user_stylesheets ;; config-list-add content.user_stylesheets css/tron-minimal.css')
config.bind('<F2>', 'spawn -u "~/.config/qutebrowser/sh/rinzler.sh" ;; config-source "py/rinzler.py" ;; config-unset content.user_stylesheets ;; config-list-add content.user_stylesheets css/rinzler-minimal.css')
config.bind('<F3>', 'spawn -u "~/.config/qutebrowser/sh/quorra.sh" ;; config-source "py/quorra.py" ;; config-unset content.user_stylesheets ;; config-list-add content.user_stylesheets css/quorra-minimal.css')

# anchor navigation
config.bind('ga', 'spawn -u ~/.config/qutebrowser/sh/anchor.sh')

# ui
c.content.user_stylesheets = ['css/tron-minimal.css']
config.source('py/tron.py')
