#!/bin/bash

# find anchors
anchor=$(
grep -Eo '<a href="#[^"]+">' $QUTE_HTML |
cut -d '#' -f2 |
cut -d '"' -f1 |
rofi -dmenu -p "anchor")

# default to "top"
[ -z "$anchor" ] && anchor='top'

# do not reload page if anchor is the same
if [[ "$QUTE_URL" == *"#$anchor" ]]; then
  echo "message-info 'Already at #$anchor'." >> "$QUTE_FIFO"
else
  echo "scroll-to-anchor $anchor" >> "$QUTE_FIFO"
fi
