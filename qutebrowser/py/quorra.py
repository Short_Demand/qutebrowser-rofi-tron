import os
os.system('sh ~/.config/qutebrowser/sh/quorra.sh')

black = "#000000"
gray = "#bebebe"
white = "#ffffff"
red = "#ff0000"
yellow = "#daa520"
green = "#8ec07c"

c.colors.completion.fg = gray
c.colors.completion.odd.bg = black
c.colors.completion.even.bg = black
c.colors.completion.category.fg = white
c.colors.completion.category.bg = black
c.colors.completion.category.border.top = gray
c.colors.completion.category.border.bottom = gray
c.colors.completion.item.selected.fg = white
c.colors.completion.item.selected.bg = black
c.colors.completion.item.selected.match.fg = yellow
c.colors.completion.item.selected.border.top = white
c.colors.completion.item.selected.border.bottom = white
c.colors.completion.match.fg = white
c.colors.completion.scrollbar.fg = gray
c.colors.completion.scrollbar.bg = black

c.colors.downloads.bar.bg = black
c.colors.downloads.start.fg = white
c.colors.downloads.start.bg = black
c.colors.downloads.stop.fg = white
c.colors.downloads.stop.bg = black
c.colors.downloads.error.fg = red

c.colors.hints.fg = gray
c.colors.hints.bg = black
c.hints.border = '2px solid' + gray
c.colors.hints.match.fg = white

c.colors.keyhint.fg = gray
c.colors.keyhint.suffix.fg = white
c.colors.keyhint.bg = black

c.colors.messages.error.fg = red
c.colors.messages.error.bg = black
c.colors.messages.error.border = red
c.colors.messages.warning.fg = yellow
c.colors.messages.warning.bg = black
c.colors.messages.warning.border = yellow
c.colors.messages.info.fg = gray
c.colors.messages.info.bg = black
c.colors.messages.info.border = black

c.colors.prompts.fg = gray
c.colors.prompts.bg = black
c.colors.prompts.border = '1px solid' + gray
c.colors.prompts.selected.bg = gray

c.colors.statusbar.normal.fg = white
c.colors.statusbar.normal.bg = black
c.colors.statusbar.insert.fg = white
c.colors.statusbar.insert.bg = black
c.colors.statusbar.passthrough.fg = green
c.colors.statusbar.passthrough.bg = black
c.colors.statusbar.private.fg = yellow
c.colors.statusbar.private.bg = black
c.colors.statusbar.command.fg = white
c.colors.statusbar.command.bg = black
c.colors.statusbar.command.private.fg = gray
c.colors.statusbar.command.private.bg = black
c.colors.statusbar.caret.fg = yellow
c.colors.statusbar.caret.bg = black
c.colors.statusbar.caret.selection.fg = white
c.colors.statusbar.caret.selection.bg = black
c.colors.statusbar.progress.bg = gray
c.colors.statusbar.url.fg = gray
c.colors.statusbar.url.error.fg = red
c.colors.statusbar.url.hover.fg = gray
c.colors.statusbar.url.success.http.fg = white
c.colors.statusbar.url.success.https.fg = white
c.colors.statusbar.url.warn.fg = yellow

c.colors.tabs.bar.bg = black
c.colors.tabs.indicator.start = gray
c.colors.tabs.indicator.stop = gray
c.colors.tabs.indicator.error = red
c.colors.tabs.odd.fg = white
c.colors.tabs.odd.bg = black
c.colors.tabs.even.fg = white
c.colors.tabs.even.bg = black
c.colors.tabs.selected.odd.fg = gray
c.colors.tabs.selected.odd.bg = black
c.colors.tabs.selected.even.fg = gray
c.colors.tabs.selected.even.bg = black
