import os
os.system('sh ~/.config/qutebrowser/sh/rinzler.sh')

black = "#000000"
orange = "#f87431"
light_orange = "#e18b6b"
red = "#ff0000"
yellow = "#daa520"
green = "#8ec07c"

c.colors.completion.fg = orange
c.colors.completion.odd.bg = black
c.colors.completion.even.bg = black
c.colors.completion.category.fg = light_orange
c.colors.completion.category.bg = black
c.colors.completion.category.border.top = orange
c.colors.completion.category.border.bottom = orange
c.colors.completion.item.selected.fg = light_orange
c.colors.completion.item.selected.bg = black
c.colors.completion.item.selected.match.fg = yellow
c.colors.completion.item.selected.border.top = light_orange
c.colors.completion.item.selected.border.bottom = light_orange
c.colors.completion.match.fg = light_orange
c.colors.completion.scrollbar.fg = orange
c.colors.completion.scrollbar.bg = black

c.colors.downloads.bar.bg = black
c.colors.downloads.start.fg = light_orange
c.colors.downloads.start.bg = black
c.colors.downloads.stop.fg = light_orange
c.colors.downloads.stop.bg = black
c.colors.downloads.error.fg = red

c.colors.hints.fg = orange
c.colors.hints.bg = black
c.hints.border = '2px solid' +  orange
c.colors.hints.match.fg = light_orange

c.colors.keyhint.fg = orange
c.colors.keyhint.suffix.fg = light_orange
c.colors.keyhint.bg = black

c.colors.messages.error.fg = red
c.colors.messages.error.bg = black
c.colors.messages.error.border = red
c.colors.messages.warning.fg = yellow
c.colors.messages.warning.bg = black
c.colors.messages.warning.border = yellow
c.colors.messages.info.fg = orange
c.colors.messages.info.bg = black
c.colors.messages.info.border = black

c.colors.prompts.fg = orange
c.colors.prompts.bg = black
c.colors.prompts.border = '1px solid' + orange
c.colors.prompts.selected.bg = orange

c.colors.statusbar.normal.fg = light_orange
c.colors.statusbar.normal.bg = black
c.colors.statusbar.insert.fg = light_orange
c.colors.statusbar.insert.bg = black
c.colors.statusbar.passthrough.fg = green
c.colors.statusbar.passthrough.bg = black
c.colors.statusbar.private.fg = yellow
c.colors.statusbar.private.bg = black
c.colors.statusbar.command.fg = light_orange
c.colors.statusbar.command.bg = black
c.colors.statusbar.command.private.fg = orange
c.colors.statusbar.command.private.bg = black
c.colors.statusbar.caret.fg = yellow
c.colors.statusbar.caret.bg = black
c.colors.statusbar.caret.selection.fg = light_orange
c.colors.statusbar.caret.selection.bg = black
c.colors.statusbar.progress.bg = orange
c.colors.statusbar.url.fg = orange
c.colors.statusbar.url.error.fg = red
c.colors.statusbar.url.hover.fg = orange
c.colors.statusbar.url.success.http.fg = light_orange
c.colors.statusbar.url.success.https.fg = light_orange
c.colors.statusbar.url.warn.fg = yellow

c.colors.tabs.bar.bg = black
c.colors.tabs.indicator.start = orange
c.colors.tabs.indicator.stop = orange
c.colors.tabs.indicator.error = red
c.colors.tabs.odd.fg = light_orange
c.colors.tabs.odd.bg = black
c.colors.tabs.even.fg = light_orange
c.colors.tabs.even.bg = black
c.colors.tabs.selected.odd.fg = orange
c.colors.tabs.selected.odd.bg = black
c.colors.tabs.selected.even.fg = orange
c.colors.tabs.selected.even.bg = black
