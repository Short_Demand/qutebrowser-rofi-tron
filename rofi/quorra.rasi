/**
 * Adapted from Monokai
 */

* {
    background:                  rgba ( 0, 0, 0, 100 % ); // transparency
    background-color:            rgba ( 0, 0, 0, 0 % );
    foreground:                  rgba ( 255, 255, 255, 100 % );
    urgent-background:           rgba ( 39, 40, 34, 0 % );
    active-foreground:           rgba ( 166, 226, 42, 100 % );
    selected-urgent-foreground:  rgba ( 248, 248, 242, 100 % );
    urgent-foreground:           rgba ( 249, 38, 114, 100 % );
    alternate-urgent-background: rgba ( 39, 40, 34, 100 % );
    alternate-active-background: rgba ( 39, 40, 34, 100 % );
    selected-urgent-background:  rgba ( 249, 38, 114, 100 % );
    active-background:           rgba ( 255, 0, 0, 100 % );
    selected-normal-background:  @foreground;
    selected-active-background:  @foreground;
    alternate-normal-foreground: @foreground;
    border-color:                @foreground;
    alternate-urgent-foreground: @urgent-foreground;
    alternate-active-foreground: @active-foreground;
    selected-normal-foreground:  @background;
    normal-foreground:           @foreground;
    normal-background:           @background;
    alternate-normal-background: @background-color;
    selected-active-foreground:  @background-color;
    bordercolor:                 @foreground;
    separatorcolor:              @foregroung;

    spacing:                     0;
}

#window {
    background-color: @background;
    border:           2;
    padding:          2;
}

#mainbox {
    border:  1;
    padding: 0;
}

#message {
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
    padding:      2px ;
}

#textbox {
    text-color: @foreground;
}

#listview {
    fixed-height: 0;
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
    spacing:      2px ;
    scrollbar:    true;
    padding:      2px 0px 0px ;
}

#element {
    border:  0;
    padding: 0px ;
}

#element.normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}

#element.normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}

#element.normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}

#element.selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}

#element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}

#element.selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}

#element.alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}

#element.alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}

#element.alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}

#scrollbar {
    width:        4px ;
    border:       0;
    handle-width: 4px ;
    padding:      0;
	handle-color: @normal-foreground;
	color: @separatorcolor;
}

#sidebar {
    border:       2px 0px 0px ;
    border-color: @separatorcolor;
}

#button.selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}

#inputbar {
    spacing:    0;
    text-color: @normal-foreground;
    padding:    1px ;
}

#case-indicator {
    spacing:    0;
    text-color: @normal-foreground;
}

#entry {
    spacing:    0;
    text-color: @normal-foreground;
}

#prompt {
    spacing:    0;
    text-color: @normal-foreground;
}

#inputbar {
    children:   [ prompt,textbox-prompt-colon,entry,case-indicator ];
}

#textbox-prompt-colon {
    expand:     false;
    str:        ":";
    margin:     0px 0.3em 0em 0em ;
    text-color: @normal-foreground;
}
